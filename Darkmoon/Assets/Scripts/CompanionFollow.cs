﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompanionFollow : MonoBehaviour {

	private Rigidbody2D vey;

	[SerializeField]
	private Transform target;

	[SerializeField]
	private float speed;

	[SerializeField]
	private float followRange;

	[SerializeField]
	private float chaseDistance;

	[SerializeField]
	private float jumpForce;


	void Start() {
		vey = GetComponent<Rigidbody2D> ();
	}

	// Update is called once per frame
	void FixedUpdate () {
		
		Movement ();
	}

	private void Movement() {
		// Vey Follows Derrick
		// Get distance to target and check to see if far enough to begin following
		float distanceToTarget = Vector2.Distance(transform.position,target.position);
		// Debug.Log ("Distance: " + distanceToTarget);

		// If further away than follow range, begin following
		if (distanceToTarget > chaseDistance ) {
			Debug.Log ("FOLLOW");
			vey.AddForce (new Vector2 (speed, jumpForce));
		} else if(distanceToTarget > followRange) {
			Debug.Log ("FOLLOW");
			vey.AddForce (new Vector2 (speed, 0));
		}
	}

}
