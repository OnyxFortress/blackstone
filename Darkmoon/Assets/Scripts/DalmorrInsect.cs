﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DalmorrInsect : MonoBehaviour {

	private Rigidbody2D bug;
	private Vector2 move = new Vector2(1,0);

	[SerializeField]
	private float speed;

	[SerializeField]
	private float maxSpeed;

	[SerializeField]
	private int health;

	[SerializeField]
	private float shield;

	[SerializeField]
	private int damage;

	[SerializeField]
	private GameObject player;

	[SerializeField]
	private float chaseDistance;

	[SerializeField]
	private float pounceDistance;

	// Use this for initialization
	void Start () {
		bug = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		Tracking ();

		// Insect dies if health gets below 0
		if (health <= 0) {
			this.gameObject.SetActive (false);
		}
	}

	void OnTriggerEnter2D(Collider2D damage) {
		if (damage.gameObject.tag == "Bullet") {
			Debug.Log ("Bullet Impact");
			Destroy(damage.gameObject);
			if (shield > 0) {
				shield -= 1;
			}
			else {
				health -= 1;
			}
			if (health == 0) {
				Destroy(gameObject);
			}
		}
		if(damage.gameObject.tag == "Sword") {
			Debug.Log ("Hit");
			if (shield == 1) {
				shield -= 1;
				health -= 1;
			}
			else if (shield >= 2) {
				shield -= 2;
			}
			else {
				health -= 2;
			}
			if (health <= 0) {
				Destroy(gameObject);
			}
		}
	}

	// Tracks the player and chases them
	void Tracking(){
		float distanceToTarget = Vector2.Distance(transform.position,player.transform.position);

		// If enemy detects derrick, begin chasing
		if (distanceToTarget < chaseDistance ) {
			Debug.Log ("CHASE");
			bug.velocity = (bug.velocity.x > maxSpeed) ? new Vector2 (maxSpeed, bug.velocity.y) : bug.velocity;
			bug.velocity = (bug.velocity.x < maxSpeed) ? new Vector2 (-maxSpeed, bug.velocity.y) : bug.velocity;
		}
	}

	void Pounce(){
		float distanceToTarget = Vector2.Distance(transform.position, player.transform.position);

		// If close enough, poune em
		if (distanceToTarget < pounceDistance ) {
			Debug.Log ("POUNCE");
			player.GetComponent<Player>().Damage(damage);
		}
	}
}
