﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShip : MonoBehaviour {

	private Rigidbody2D ship;

	[SerializeField]
	private float speed;
	public GameObject shipBullet;
	private float shoot;
	private bool fired;
	private bool fireLeft;

	// Use this for initialization
	void Start () {
		ship = GetComponent<Rigidbody2D> ();
		fired = true;
		fireLeft = true;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float horizontal = Input.GetAxis ("KeyboardMovement");
		float vertical = Input.GetAxis ("KeyboardVertical");
		shoot = Input.GetAxis ("KeyboardShoot");
		if (Input.GetJoystickNames().Length == 0) {
			horizontal = Input.GetAxis ("KeyboardMovement");
			vertical = Input.GetAxis ("KeyboardVertical");
			shoot = Input.GetAxis ("KeyboardShoot");
		}
		else {
			horizontal = Input.GetAxis ("Horizontal");
			vertical = Input.GetAxis ("Vertical");
			shoot = Input.GetAxis ("Shoot");
		}


		Movement (horizontal, vertical);

		if(shoot == 1 && fired == true) {
			if (fireLeft == true) {
				Instantiate(shipBullet, new Vector3(transform.position.x - 3, transform.position.y + 18.2f, transform.position.z), new Quaternion(0, 0, 0, 0));
				fireLeft = false;
			}
			else if (fireLeft == false) {
				Instantiate(shipBullet, new Vector3(transform.position.x + 3, transform.position.y + 18.2f, transform.position.z), new Quaternion(0, 0, 0, 0));
				fireLeft = true;
			}
			fired = false;
			StartCoroutine(bulletShooter(0.1f)); 
		}
	}

	IEnumerator bulletShooter(float wait){
//			yield return new WaitForSeconds(wait/2);
//			Instantiate(shipBullet, new Vector3(transform.position.x + 3, transform.position.y + 18.2f, transform.position.z), new Quaternion(0, 0, 0, 0));
			yield return new WaitForSeconds(wait);
			fired = true;
	}

	private void Movement(float horizontal, float vertical){
		ship.velocity = new Vector2(horizontal * speed, ship.velocity.y);
		ship.velocity = new Vector2 (ship.velocity.x, -1* vertical * speed);
	}
}
