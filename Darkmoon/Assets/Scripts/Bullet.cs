﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	[SerializeField]
	private float speed;
	private GameObject derrick;

	// Use this for initialization
	void Start () {
		derrick = GameObject.FindGameObjectWithTag("Player");
		if (derrick.transform.localScale.x < 0) {
			float newDirection = 0;
			speed *= -1;
			print(transform.localScale.x);
			newDirection = transform.localScale.x * -1;
			transform.localScale = new Vector3(newDirection, transform.localScale.y, transform.localScale.z);
			print(transform.localScale.x);
		}
		GetComponent<Rigidbody2D>().velocity = new Vector2(speed, transform.position.y);
		StartCoroutine(Dead(4));
	}

	IEnumerator Dead (float wait) {
		yield return new WaitForSeconds(wait);
		if(gameObject != null) {
			Destroy(gameObject);
		}		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
