﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampanionFollow : MonoBehaviour {

	private Rigidbody2D vey;

	public Transform target;
	public Transform veyTransform;
	public float speed;
	public float followRange;


	void Start() {
		vey = GetComponent<Rigidbody2D> ();
	}

	// Update is called once per frame
	void Update () {

		// Vey Follows Derrick
		// Get distance to target and check to see if far enough to begin following
		float distanceToTarget = Vector2.Distance(transform.position,target.position);
		// Debug.Log ("Distance: " + distanceToTarget);

		// If further away than follow range, begin following
		if (distanceToTarget > followRange) {
			Debug.Log ("FOLLOW");
			vey.AddForce(new Vector2(speed,0));
		}



	}

}
