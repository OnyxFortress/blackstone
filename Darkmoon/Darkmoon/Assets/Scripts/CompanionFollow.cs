﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompanionFollow : MonoBehaviour {

	private Rigidbody2D vey;

	[SerializeField]
	private Transform target;

	private Player derrick;

	[SerializeField]
	private float speed;

	[SerializeField]
	private float followRange;

	[SerializeField]
	private float chaseDistance;

	[SerializeField]
	private float stopRange;

	[SerializeField]
	private float jumpForce;

	[SerializeField]
	private float distanceToTarget;


	void Start() {
		vey = GetComponent<Rigidbody2D> ();
		derrick = target.gameObject.GetComponent<Player>();
	}

	// Update is called once per frame
	void FixedUpdate () {
		
		Movement ();
	}

	private void Movement() {
		// Vey Follows Derrick
		// Get distance to target and check to see if far enough to begin following
		if(transform.position.y <= target.position.y + 10 || transform.position.y >= target.position.y - 10) {
			distanceToTarget = Vector2.Distance(transform.position, target.position);
		}
		else {
			distanceToTarget = Vector2.Distance(new Vector2(transform.position.x, 0), new Vector2(target.position.x, 0));
		}

		// If further away than follow range, begin following
		if (distanceToTarget > chaseDistance && vey.velocity.x <= 75) {
			Debug.Log ("FOLLOW");
			vey.AddForce (new Vector2 (speed * 1.1f, jumpForce));
		} else if(distanceToTarget > followRange && vey.velocity.x <= 75) {
			vey.drag = 0;
			Debug.Log ("FOLLOW");
			vey.AddForce (new Vector2 (speed, 0));
		}
		if(transform.position.x >= target.position.x - 15 && !(transform.position.x >= target.position.x - 10) && !derrick.Direction()) {
			vey.velocity = new Vector2(15, vey.velocity.y);

		}
		else if(transform.position.x >= target.position.x - 10) {
			Debug.Log (vey.velocity.x);
			vey.AddForce (new Vector2 (-speed * 2, 0));
			if (vey.velocity.x <= -20) {
				vey.velocity = new Vector2(-20, vey.velocity.y);
			}
		}
		if(vey.velocity.x >= 75) {
			vey.velocity = new Vector2(60, vey.velocity.y);
		}
	}

}
