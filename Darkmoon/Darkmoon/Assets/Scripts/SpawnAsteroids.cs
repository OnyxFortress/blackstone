﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAsteroids : MonoBehaviour {

	public GameObject[] asteroids;

	public float waitingForNextSpawn = 10;
	public float countdown = 10;

	public float xMin;
	public float xMax;

	public float yMin;
	public float yMax;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	public void Update () {
		countdown -= Time.deltaTime;
		if (countdown <= 0) {
			// spawnNewAsteroid();
			countdown = waitingForNextSpawn;
		}
	}

	void spawnNewAsteroid() {
		// Defines range for X and Y
		Vector2 pos = new Vector2 (Random.Range (xMin, xMax), Random.Range (yMin, yMax));

		// Choose new asteroid to spawn
		GameObject asteroidPrefab = asteroids [Random.Range (0, asteroids.Length)];

//		Instantiate (asteroidPrefab,this);
	}
}
