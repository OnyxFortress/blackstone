﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroids : MonoBehaviour {

	[SerializeField]
	private int health;

	[SerializeField]
	private GameObject player;

	[SerializeField]
	private float damageAmount;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D damage) {
		if (damage.gameObject.tag == "Missle") {
			Debug.Log ("Missle Impact");
			Destroy(damage.gameObject);
			health -= 1;
			if (health == 0) {
				Destroy(gameObject);
			}
		}
		else if(damage.gameObject.tag == "Player") {
			Debug.Log ("Player");
			player.GetComponent<PlayerShip>().Damage(damageAmount);
			Destroy(gameObject);

		}
	}
}
