﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBullet : MonoBehaviour {

	[SerializeField]
	private float speed;
	private GameObject ship;

	// Use this for initialization
	void Start () {
		ship = GameObject.FindGameObjectWithTag("Player");
		GetComponent<Rigidbody2D>().velocity = new Vector2(transform.position.x, speed);
		StartCoroutine(Dead(3));
	}

	IEnumerator Dead (float wait) {
		yield return new WaitForSeconds(wait);
		if(gameObject != null) {
			Destroy(gameObject);
		}		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
