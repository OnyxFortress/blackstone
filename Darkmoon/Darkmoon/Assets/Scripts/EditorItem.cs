﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorItem : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//makes the object invisible but still in the scene.
		gameObject.GetComponent<Renderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
