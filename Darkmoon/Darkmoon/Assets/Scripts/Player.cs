﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	private Rigidbody2D derrick;

	[SerializeField]
	private float speed;

	[SerializeField]
	private int health;
	private int shield;
	private int shieldMax;
	private int healthMax;

	[SerializeField]
	private bool facingLeft;
	private bool fired;

	private bool doubleJump;
	private int jumpCount;

	[SerializeField]
	private GameObject bullet;

	[SerializeField]
	private float jumpForce;

	[SerializeField]
	private float reload;

	private float shoot;
	private int shotsFired;

	private float swordAttack;
	private Collider2D swordCollider;
	public GameObject swordReference;

	private float horizontal;
	private float dash;
	private bool dashed;
	private float tempDash;
	private bool dashReload;

	public Animator animator;


	// Use this for initialization
	void Start () {
		derrick = GetComponent<Rigidbody2D> ();
		health = 5;
		healthMax = 5;
		shield = 1;
		shieldMax = 1;
		reload = 0.5f;
		doubleJump = false;
		fired = false;
		dashed = false;
		dashReload = false;
		jumpCount = 0;
		tempDash = 0;
		//swordReference = GameObject.FindGameObjectWithTag("Sword");
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		horizontal = Input.GetAxis ("KeyboardMovement");
		shoot = Input.GetAxis ("KeyboardShoot");
		swordAttack = Input.GetAxis("KeyboardMelee");
		dash = Input.GetAxis("KeyboardDash");

		if (Input.GetJoystickNames().Length == 0) {
			horizontal = Input.GetAxis ("KeyboardMovement");
			shoot = Input.GetAxis ("KeyboardShoot");
			swordAttack = Input.GetAxis("KeyboardMelee");
			dash = Input.GetAxis("KeyboardDash");
		}
		else {
			horizontal = Input.GetAxis ("Horizontal");
			shoot = Input.GetAxis ("Shoot");
			swordAttack = Input.GetAxis("Melee");
			dash = Input.GetAxis("Dash");
		}


		Movement (horizontal);
		Flip (horizontal);

		if(shoot == 1 && fired == false) {
			if (animator.GetBool("Gun") == false) {
				animator.SetBool("Gun", true);
			}
			fired = true;
			StartCoroutine(bulletShooter(0.1f)); 
			Instantiate(bullet, transform.position, new Quaternion(0, 0, 0, 0));
		}


		if (swordAttack > 0) {
			if(swordReference != null) {
				swordReference.GetComponent<Collider2D>().enabled = true;
				if (animator.GetBool("Sword") == false) {
					animator.SetBool("Sword", true);
				}
			}
		}
		else if (swordAttack == 0 && swordReference.GetComponent<Collider2D>().enabled == true) {
			swordReference.GetComponent<Collider2D>().enabled = false;
			if (animator.GetBool("Sword") == true) {
				animator.SetBool("Sword", false);
			}
		}

	}

	IEnumerator bulletShooter(float wait){
		
		if(shotsFired == 4) {
			wait = reload;
			yield return new WaitForSeconds(wait);
			if (animator.GetBool("Gun") == true) {
				animator.SetBool("Gun", false);
			}
			fired = false;
			shotsFired = 0;
		}
		else if(shotsFired < 4) {
			yield return new WaitForSeconds(wait);
			shotsFired++;
			fired = false;
			if (animator.GetBool("Gun") == true) {
				animator.SetBool("Gun", false);
			}
		}

	}

	// Handles Derrick's movement
	private void Movement(float horizontal){

		if(dash > 0 && dashed == false && dashReload == false) {
			dashed = true;
			dashReload = true;
			tempDash = 1;
			StartCoroutine(dashCounter(0.15f));
			StartCoroutine(dashCoolDown(1.5f));
		}
		if(dashed == true) {
			tempDash = Mathf.Lerp(tempDash, 0, 0.18f);
			if (facingLeft) {
				derrick.velocity = new Vector2(-tempDash * 700, -55);
			}
			else {
				derrick.velocity = new Vector2(tempDash * 700, -55);
			}
		}
		else {
			derrick.velocity = new Vector2(horizontal * speed, derrick.velocity.y);
		}



		if (Input.GetJoystickNames().Length == 0) {
			if(Input.GetButtonUp("KeyboardJump") && jumpCount < 1){
				derrick.velocity = new Vector2(derrick.velocity.x, jumpForce);
				jumpCount += 1;
			}
			else if (Input.GetButtonUp("KeyboardJump") && jumpCount < 2) {
				derrick.velocity = new Vector2(derrick.velocity.x, jumpForce * 1.5f);
				jumpCount += 1;
			}
		}

		else {
			if(Input.GetButtonUp("Jump") && jumpCount < 1){
				derrick.velocity = new Vector2(derrick.velocity.x, jumpForce);
				jumpCount += 1;
			}
			else if (Input.GetButtonUp("Jump") && jumpCount < 2) {
				derrick.velocity = new Vector2(derrick.velocity.x, jumpForce * 1.5f);
				jumpCount += 1;
			}
		}

	}

	void OnCollisionEnter2D(Collision2D collision) {
		print(collision.gameObject);
		if (collision.gameObject.tag == "Ground") {
			jumpCount = 0;
		}
	}

	void OnTriggerEnter2D(Collider2D collision) {
		if (collision.gameObject.tag == "PickUp"){
			if (collision.gameObject.name == "shield-pickup") {
				shieldMax += 1;
				shield = shieldMax;
			}
			if (collision.gameObject.name == "health-pickup") {
				if (health == healthMax) {
					healthMax += 1;
				}
				else {
					health += 1;
				}
			}
			Destroy(collision.gameObject);
		}
	}

	IEnumerator shieldRegenerationStart(float wait) {
		yield return new WaitForSeconds(wait);
		shield += 1;
		if(shield < shieldMax) {
			StartCoroutine(shieldRegeneration(0.3f));
		}
	}

	IEnumerator shieldRegeneration(float wait) {
		yield return new WaitForSeconds(wait);
		shield += 1;
		if(shield < shieldMax) {
			StartCoroutine(shieldRegeneration(wait));
		}
	}


	// Flips the sprite the correct direction
	private void Flip (float horizontal){
		if(horizontal < 0 && !facingLeft || horizontal > 0 && facingLeft)
		{
			facingLeft = !facingLeft;
			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale = theScale;
		}
	}

	IEnumerator dashCounter(float wait) {
		yield return new WaitForSeconds(wait);
		dashed = false;
	}

	IEnumerator dashCoolDown(float wait) {
		yield return new WaitForSeconds(wait);
		dashReload = false;
	}
		
	public void Damage(int amount) {
		if (shield >= 1) {
			if (amount > shield) {
				shield = shield - shield;
				amount = amount - shield;
				if (health < amount) {
					health = 0;
				}
				else {
					health = health - amount;
				}
			}
			else if (amount == shield) {
				shield = 0;
			}
		}
		if (health < amount) {
			health = 0;
		}
		else {
			health = health - amount;
		}
		if (shield < shieldMax) {
			StartCoroutine(shieldRegenerationStart(1.5f));
		}
		if (health == 0) {
			// temporary
			Time.timeScale = 0;
		}
	}

	// Get if facing Left
	public bool Direction() {
		return facingLeft;
	}
}
